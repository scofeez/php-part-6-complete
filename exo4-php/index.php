<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <a href="?langage=PHP&amp;serveur=LAMP" title="lien de test">Utilitaire de programmation utilisé?</a>
      <?php
          if(isset($_GET['langage']) && isset($_GET['serveur'])) {
              echo '<br/>Le ' . $_GET['langage'] . ' est utilisé sur cette page, basé sur un serveur ' . $_GET['serveur'] . '.';
          }
          else {
              echo '<br/>Le langage et le type de serveur utilisés restent à définir.';
          }
      ?>
  </body>
</html>
