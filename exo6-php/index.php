<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <a href="?batiment=12&amp;salle=101" title="lien de test">Lieu de la formation E2N</a>
      <?php
          if(isset($_GET['batiment']) && isset($_GET['salle'])) {
              echo '<br/>La formation E2N se déroule à Inovia dans le bâtiment ' . $_GET['batiment'] . ', en salle ' . $_GET['salle'] . '.';
          }
          else {
              echo '<br/>Le lieu exact de la formation E2N n\'a pas été défini.';
          }
      ?>
  </body>
</html>
