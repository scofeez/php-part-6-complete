<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <a href="?username=GargamelBG80&amp;town=Poudlard" title="Lien">Comment tu t'appelles et où habites tu ?</a>
      <?php
          // le isset vérifi que le tableau associatif existe bien (bool)
          if(isset($_GET['username']) && isset($_GET['town'])) {
              echo '<br/>Vous vous appelez ' . $_GET['username'] . ' et venez de ' . $_GET['town'];
          }
          else {
              echo '<br/>Veuillez renseigner des informations valides.';
          }
      ?>
  </body>
</html>
